$(document).ready(function() {
  /* Tutorial creators should edit the title, author, and list of lesson
     name values appropriately. */
  var title =  "Manos a la obra con la CLI de Unix";
  var author = "Daniel Lancini, Jeffrey Elkner, and Adrian Buchholz";
  var lessons = [
                "Cual es la CLI de Unix?",
                "Archivos y el sistema de archivos",
                "Editar archivos con GNU nano",
                "Los comandos rm, cp y mv",
                "Editar archivos con Vim",
                "Conexión remota con ssh, scp y rsync",
                "Localización de recursos del sistema de archivos con find",
                "Búsqueda de texto dentro de archivos usando grep",
                "A dónde ir desde aquí"
                ];

  /* Don't touch anything below here unless you really know what you
     are doing! */
  $('title').text(title);
  $('header h1 a').text(title);
  $('p#author span').text(author);

  var digit = document.URL.substr(-6, 1);
  if (digit >= '0' && digit <= '9') {
    var changed = "s" + digit;
    var num = parseInt(digit);
    document.getElementById(changed).style.background = "#006";
    $('h1#title').text("Lección " + digit + ": " + lessons[num - 1]);
    $('p#finished span').text(digit);
    if (num < 9) {
      var nlink = "lección" + (num + 1) + ".html";
      $("a#next").attr('href', nlink);
    };
  } else if (digit == 'x') {
    for (var i = 1; i < 10; i++) {
      $('li#l' + i + ' span').text(lessons[i - 1]);
    };
  };
});
