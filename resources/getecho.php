<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>GET Echo</title>
<style type="text/css">
body {
    margin: 50px;
}
h1 {
    text-align: center;
}
table {
    font-size: large;
    width: 800px;
    margin-left: auto;
    margin-right: auto;
}
td, th {
    padding: 5px;
    text-align: right;
}
td+td, th+th {
    text-align: left;
}
td:first-child:after {
    content: " : ";
}
a, a:visited {
    text-decoration: none;
    color: #777;
}
footer {
    text-align: center;
    margin-top: 40px;
}
</style>
</head>
<body>
<h1>GET Echo</h1>
<p>
This page echos (delivers back to the user who submitted it) form data from
an HTML GET request.
</p>
<table>
<tr>
  <th>name</th><th>value</th>
</tr>
<?php
  $_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
  while (list ($key, $val) = each($_GET))
      echo "<tr>\n  <td>$key</td><td>$val</td>\n</tr>\n";
?>
</table>
<p>
<a href="http://php.net/">PHP</a> for this page kindly provided by my friend
and college, <a href="http://www.nvcc.edu/home/kreed/">Prof. Kevin Reed</a>
of <a href="http://www.nvcc.edu">Northern Virginia Community College</a>.
</p>

<footer>
<a href="http://validator.w3.org/check/referer">
<strong> HTML </strong> Valid! </a> |
<a href="http://jigsaw.w3.org/css-validator/check/referer?profile=css3">
<strong> CSS </strong> Valid! </a>
</footer>

</body>
</html>
