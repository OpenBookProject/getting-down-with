# Getting Down with Series

A series of short, *learn by doing* tutorials on web development and computer
science topics created by and for the educational community in
[Arlington Public Schools](https://www.apsva.us/).

Each tutorial is 9 lessons long, with the 9th lesson being a *what's next*
or *where to go from here* list of links for further study.
